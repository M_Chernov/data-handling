package com.company.Exercises;


import java.time.LocalDate;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Dates {

    public void exOne() {
        LocalDate dayOfBirth = LocalDate.of(1996, 8, 14);
        LocalDate date = LocalDate.now();
        LocalTime now = LocalTime.now();
        Period period = Period.between(dayOfBirth, date);
        System.out.println(period.getYears() + "-" + period.getMonths() + "-" + period.getDays() + " " + now);
    }

    public void exTwo() {
        long date1 = new Date(2010, 1, 14).getTime();
        long date2 = new  Date(2010, 4, 14).getTime();
        long period = date2 - date1;
        System.out.println(period/(1000*60*60*24));
    }

    public void exThree() {
        String stringDate = "Friday, Aug 12, 2016 00:10:56 PM";
        String oldDateFormat = "EEEE, MMM dd, yyyy hh:mm:ss a";
        String newDateFormat = "yyyy-MM-dd";
        DateTimeFormatter oldFormatter = DateTimeFormatter.ofPattern(oldDateFormat, Locale.ENGLISH);
        DateTimeFormatter newFormatter = DateTimeFormatter.ofPattern(newDateFormat, Locale.ENGLISH);
        LocalDate date = LocalDate.parse(stringDate, oldFormatter);
        String newDate = date.format(newFormatter);
        System.out.println(newDate);
    }
}
