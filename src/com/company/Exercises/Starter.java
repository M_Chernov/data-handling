package com.company.Exercises;

public class Starter {

    public void start() {
        System.out.println("Первое задание");
        Dates dates = new Dates();
        dates.exOne();
        dates.exTwo();
        dates.exThree();
        System.out.println();
        System.out.println("Второе задание");
        Others others = new Others();
        others.exOne();
        others.exTwo();
        others.exThree();
    }
}
