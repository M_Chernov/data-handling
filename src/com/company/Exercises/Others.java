package com.company.Exercises;

import java.util.Arrays;

public class Others {

    public void exOne() {
        int a = 100;
        double s = Math.PI * a * a;
        System.out.format("S = %.50f", s);
        System.out.println();
    }

    public void exTwo() {
        String stringOfNumbers = "0.2, 0.5, 0.7";
        double[] numbers = Arrays.stream(stringOfNumbers.split(", ")).mapToDouble(Double::parseDouble).toArray();
        System.out.println(numbers[0] + numbers[1] == numbers[2]);
    }

    public void exThree() {
        int[] numbers = new int[3];
        numbers[0] = 3;
        numbers[1] = 5;
        numbers[2] = 2;
        System.out.println(Arrays.stream(numbers).min());
        System.out.println(Arrays.stream(numbers).max());
    }
}
